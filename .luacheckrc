--mostly stolen from the project repos, with minor edits to make them make
--sense for this config repo, e.g. removing stuff related to the project source
--code itself, tests, etc.



-- Enable cache (uses .luacheckcache relative to this rc file).
cache = true
-- This file itself
files[".luacheckrc"].ignore = {"111", "112", "131"}



--awesomewm
files["modules/desktop/awesome"] = {
    -- Only allow symbols available in all Lua versions
    std = "min"

    -- Get rid of "unused argument self"-warnings
    self = false

    -- The unit tests can use busted
    -- files["spec"].std = "+busted"

    -- Global objects defined by the C code
    read_globals = {
        "awesome",
        "button",
        "dbus",
        "drawable",
        "drawin",
        "key",
        "keygrabber",
        "mousegrabber",
        "selection",
        "tag",
        "window",
        "table.unpack",
        "math.atan2",
        "client"
    }

    -- screen may not be read-only, because newer luacheck versions complain about
    -- screen[1].tags[1].selected = true.
    -- The same happens with the following code:
    --   local tags = mouse.screen.tags
    --   tags[7].index = 4
    -- client may not be read-only due to client.focus.
    globals = {
        "screen",
        "mouse",
        "root",
        "client"
    }

    -- Do not enable colors to make the CI output more readable.
    color = false

        -- The default config may set global variables
    files["rc.lua"].allow_defined_top = true

    -- Theme files, ignore max line length
    files["themes/*"].ignore = {"631"}
}


--neovim
files["modules/tui/neovim"] = {
    stds.nvim = {
        read_globals = { "jit" }
    }
    std = "lua51+nvim"

    -- Don't report unused self arguments of methods.
    self = false

    -- Rerun tests only if their modification time changed.
    cache = true

    ignore = {
        "631",  -- max_line_length
        "212/_.*",  -- unused argument, for vars with "_" prefix
        "214", -- used variable with unused hint ("_" prefix)
        "121", -- setting read-only global variable 'vim'
        "122", -- setting read-only field of global variable 'vim'
        "581", -- negation of a relational operator- operator can be flipped (not for tables)
    }

    -- Global objects defined by the C code
    read_globals = {
        "vim",
    }

    globals = {
        "vim.g",
        "vim.b",
        "vim.w",
        "vim.o",
        "vim.bo",
        "vim.wo",
        "vim.go",
        "vim.env"
    }
}



--xplr


-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
