{
  description = "nhflake";

  /*{{{*/
  inputs = {
    #main nixpkgs used for most of the system
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    #stable branch
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.05";

    #used for virtualbox, because it gets broken sometimes
    #DO NOT UPDATE THIS
    nixpkgs-vbox.url = "github:nixos/nixpkgs/nixos-unstable";

    #used for neovim
    #DO NOT UPDATE THIS UNLESS READY TO REBUILD NVIM CONFIG
    nixpkgs-2311.url = "github:nixos/nixpkgs/nixos-23.11";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager-stable = {
      url = "github:nix-community/home-manager/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs-stable";
    };

    nuenv = {
      url = "github:DeterminateSystems/nuenv";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    awesomewm-git = {
      type = "github";
      owner = "awesomeWM";
      repo = "awesome";
      flake = false;
    };

    # adw-colors = {
    #   type = "github";
    #   owner = "lassekongo83";
    #   repo = "adw-colors";
    #   flake = false;
    # };

    #virtio-win iso, i don't wanna compile this myself
    # virtio-win = {
    #   type = "file";
    #   url = "https://fedorapeople.org/groups/virt/virtio-win/direct-downloads/archive-virtio/virtio-win-0.1.248-1/virtio-win.iso";
    # };
  };
  /*}}}*/

  outputs = {
    self,
    nixpkgs,
    nixpkgs-stable,
    nixpkgs-2311,
    nixpkgs-vbox,
    home-manager,
    home-manager-stable,
    nuenv,
    sops-nix,
    awesomewm-git,
    ...
  }:
  let
    globals = rec {
      system = "x86_64-linux";
      user = "nh";
      homeDir = "/${user}";
      fullName = "Daniel";
      gitEmail = "nighth4nter@gmail.com";
      gitUser = "Daniel";
      shitBrowser = "flatpak run com.microsoft.Edge --";
      terminal = "wezterm";
      discord = "flatpak run com.discordapp.Discord";
      telegram = "flatpak run org.telegram.desktop";
      pubkeys = import ./keys/ssh-pub.nix;
    };
    pkgs-nvim = import nixpkgs-2311 { system = "${globals.system}"; };
    pkgs-vbox = import nixpkgs-vbox { system = "${globals.system}"; };
    pkgs-stable = import nixpkgs-stable { system = "${globals.system}"; };
    sops-hm = sops-nix.homeManagerModules.sops;
  in {
    nixosConfigurations = {
      nhdpc = nixpkgs.lib.nixosSystem {
        system = "${globals.system}";
        specialArgs = {
          inherit
            pkgs-stable
            pkgs-nvim
            pkgs-vbox
            globals
            sops-hm;
        };
        modules = [
          {
            nixpkgs.overlays = [
              self.overlays.awesome
              nuenv.overlays.nuenv
            ];
          }
          home-manager.nixosModules.home-manager
          sops-nix.nixosModules.sops
          ./hosts/nhdpc
          ./hosts/nhdpc/xorg.nix
          ./modules/common
        ] ++ nixpkgs.lib.attrsets.attrValues self.nixosModules;
      };
      jbod =
      let
        nixpkgs = nixpkgs-stable;
      in
      nixpkgs.lib.nixosSystem {
        system = "${globals.system}";
        specialArgs = {
          inherit globals pkgs-nvim pkgs-vbox sops-hm;
        };
        modules = [
          {
            nixpkgs.overlays = [
              self.overlays.awesome
              nuenv.overlays.nuenv
            ];
          }
          home-manager-stable.nixosModules.home-manager
          sops-nix.nixosModules.sops
          ./hosts/jbod
          ./modules/common/min.nix
          # ./modules/common
        ] ++ nixpkgs.lib.attrsets.attrValues self.nixosModules;
      };
    };

    overlays = {
      awesome = import ./overlays/awesome.nix awesomewm-git;
    };

    nixosModules = {
      nushell               = import ./modules/cli/nushell;
      git                   = import ./modules/cli/git;
      secret-management     = import ./modules/cli/secret-management;
      ssh                   = import ./modules/cli/ssh;

      xplr                  = import ./modules/tui/xplr;
      neovim                = import ./modules/tui/neovim;
      lazygit               = import ./modules/tui/lazygit;
      tmux                  = import ./modules/tui/tmux;

      greetd                = import ./modules/desktop/greetd;
      xorg                  = import ./modules/desktop/xorg;
      awesome               = import ./modules/desktop/awesome;
      redshift              = import ./modules/desktop/redshift;
      picom                 = import ./modules/desktop/picom;
      audio                 = import ./modules/desktop/audio;
      unclutter             = import ./modules/desktop/unclutter;
      hotkeys               = import ./modules/desktop/hotkeys;
      qt                    = import ./modules/desktop/qt;
      gtk                   = import ./modules/desktop/gtk;
      flameshot             = import ./modules/desktop/flameshot;
      clipmenu              = import ./modules/desktop/clipmenu;
      polkit                = import ./modules/desktop/polkit;

      fonts                 = import ./modules/gui/fonts;
      pcmanfm-qt            = import ./modules/gui/pcmanfm-qt;
      zathura               = import ./modules/gui/zathura;
      wezterm               = import ./modules/gui/wezterm;

      udisks                = import ./modules/hardware/udisks;

      flatpak               = import ./modules/compat/flatpak;

      virtualbox            = import ./modules/virtualisation/virtualbox;
      qemukvm               = import ./modules/virtualisation/qemukvm;
      docker                = import ./modules/virtualisation/docker;
      podman                = import ./modules/virtualisation/podman;

      qbittorrent           = import ./modules/services/qbittorrent;
    };
  };
}
