{ pkgs, globals, ... }:

{
  nix = {
    settings = {
      experimental-features = [ "nix-command" "flakes" ];
      auto-optimise-store = true;
      trusted-users = [ "nh" ];
    };

    gc = {
      automatic = true;
      options = "-d --delete-older-than 14d";
      dates = "monthly";
    };
  };


  nixpkgs.config = {
    allowUnfree = true;
    # allowBroken = true;
  };


  environment.systemPackages = with pkgs; [
    neovim
    wget
    curl
    tree
    htop
    iotop
    tmux
    file
  ];


  networking.firewall.enable = true;


  users.defaultUserShell = pkgs.nushell;


  users.users.${globals.user} = {
    isNormalUser = true;
    description = "${globals.fullName}";
    extraGroups = [
      "wheel"
    ];
    initialPassword = "12";
    home = "${globals.homeDir}";
  };


  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = { inherit globals; };

    users = {
      ${globals.user} = {
        home = {
          stateVersion = "23.11";
          username = "${globals.user}";
          homeDirectory = "${globals.homeDir}";
        };
        programs.nushell.enable = true;
      };

      root.home = {
        stateVersion = "23.11";
        username = "root";
        homeDirectory = "/root";
      };
    };
  };


  environment.variables = {
    EDITOR = "nvim";
    VISUAL = "nvim";
  };


  environment.shells = [ pkgs.nushell ];


  i18n = {
    defaultLocale = "en_US.UTF-8";

    extraLocaleSettings = {
      LC_MESSAGES = "en_US.UTF-8";
      LC_TIME = "en_US.UTF-8";
      LC_CTYPE = "en_US.UTF-8";
      LANGUAGE = "en_US.UTF-8";
    };

    supportedLocales = [
      "C.UTF-8/UTF-8"
      "en_US.UTF-8/UTF-8"
      "ru_RU.UTF-8/UTF-8"
      "he_IL.UTF-8/UTF-8"
      "nl_NL.UTF-8/UTF-8"
    ];
  };


  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };
}

