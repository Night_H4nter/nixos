{ config, lib, globals, ...}:

let
  pubkeys = globals.pubkeys;
in

{
  options.nh.cli.ssh.jbod-key.enable = lib.mkEnableOption "Add my jbod ssh key";
  # options.nh.cli.ssh = {
  #   keys = lib.mkOption {
  #     default = [ ];
  #     description = "Keys to add to the user";
  #     defaultText = "[ ] (none)";
  #   };
  # };

  config = lib.mkIf config.nh.cli.ssh.jbod-key.enable {
    home-manager.users.${globals.user} = {
      sops.secrets.jbod-ssh = {
        path = ".ssh/jbod";
      };
      home.file.jbod-ssh-pub = {
        text = "${pubkeys.jbod}";
        target = ".ssh/jbod.pub";
      };
    };
  };
}
