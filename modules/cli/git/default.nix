{ pkgs, globals, lib, config, ... }:

{
  options.nh.cli.git.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.cli.git.enable {
    home-manager.users.${globals.user}.programs.git = {
      enable = true;
      package = pkgs.gitAndTools.gitFull;
      userName = "${globals.gitUser}";
      userEmail = "${globals.gitEmail}";
    };
  };
}
