{ pkgs, globals, lib, config, ... }:

let
  nushell-nixos-helper = pkgs.writeText "nushell-nixos-helper.nu" ''
let bash_profile_raw = bash -c '
#source everything set up for bash and output it as is
  source /etc/profile
  source /etc/bashrc
  source ${config.home-manager.users.${globals.user}.home.profileDirectory}/etc/profile.d/hm-session-vars.sh
  env' |
  lines |
  parse "{n}={v}" |
  filter { |x| (not ($x.n in $env)) or $x.v != ($env | get $x.n) }

let bash_profile = $bash_profile_raw |
  where n not-in ["_", "LAST_EXIT_CODE", "DIRS_POSITION", "PATH"] |
  transpose --header-row |
  into record

let bash_path = $bash_profile_raw |
  where n == "PATH" |
  get v |
  split row (char esep)

$bash_profile | load-env

$env.PATH = ($env.PATH | append $bash_path | uniq)
    '';

  nushell-solarized-theme = pkgs.writeText "solarized-dark.nu"
    (builtins.readFile ./config/solarized-dark.nu);

  nushell-git-helpers = pkgs.writeText "nushell-git-helpers.nu"
    (builtins.readFile ./modules/git.nu);
in
{
  options.nh.cli.nushell.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.cli.nushell.enable {
    environment.systemPackages = [ pkgs.nushell ];

    home-manager.users.${globals.user} = {
      programs.carapace = {
        enable = true;
        enableNushellIntegration = true;
      };

      programs.nushell = {
        enable = true;
        configFile.source = ./config/config.nu;
        envFile.source = ./config/env.nu;
        extraConfig = ''
          source ${nushell-nixos-helper}
          use ${nushell-solarized-theme} solarized-dark
          $env.config = ($env.config | merge { color_config: (solarized-dark) })
          plugin add ${pkgs.nushellPlugins.polars}/bin/nu_plugin_polars
          plugin add ${pkgs.nushellPlugins.query}/bin/nu_plugin_query
          plugin add ${pkgs.nushellPlugins.formats}/bin/nu_plugin_formats
        '';
        extraEnv = ''
          use ${nushell-git-helpers} *

          def path-segment [] {
            let cwd = match (do --ignore-shell-errors { $env.PWD |
              path relative-to $nu.home-path }) {
                null => $env.PWD
                  ''' => '~'
                  $relative_pwd => ([~ $relative_pwd] | path join)
              }

            $'(ansi purple_bold)($cwd)(ansi reset)' |
              str replace --all (char path_sep) $'(ansi light_green_bold)(char path_sep)(ansi purple_bold)'
          }

          def user-sign [] = {
            if (is-admin) {
              $'(ansi light_cyan_bold)#(ansi reset)'
            } else {
              $'(ansi light_cyan_bold)~(ansi reset)'
            }
          }

          def create_left_prompt_short [] {
            $'(path-segment) (branch-styled-brief) (user-sign)' |
              str replace '  ' ' '
          }

          def create_left_prompt_full [] {
            let firstline_decoration = $'(ansi light_yellow_bold)╭─(ansi reset)'
            let secondline_decoration = $'(ansi light_yellow_bold)╰─(ansi reset)'

            let shell_reminder = $'(ansi green_bold)nushell(ansi reset)'

            let user_at_host = [
              (ansi cyan_bold)
              ($env.USER)
              (ansi light_green_bold)
              '@'
              (ansi cyan_bold)
              ((sys host).hostname)
              (ansi reset)
            ] | str join


            [
              ($firstline_decoration)
              ' '
              (path-segment)
              ' '
              (repo-styled)
              (char nl)
              ($secondline_decoration)
              ' '
              ($user_at_host)
              ' '
              ($shell_reminder)
              ' '
              (user-sign)
            ] | str join | str replace '  ' ' '
          }

          def create_right_prompt [] {
            let time_segment = $'(ansi light_green)(date now |
              format date %H:%M:%S)(ansi reset)'

            let last_exit_code = if ($env.LAST_EXIT_CODE != 0) {
              $'(ansi rb)exited: ($env.LAST_EXIT_CODE)(ansi reset)'
            } else { $'(ansi reset)' }

            let last_cmd_took = $'took: ($"($env.CMD_DURATION_MS)ms" |
              into duration)(ansi reset)'

            $'($last_exit_code) ($last_cmd_took) ($time_segment)'|
              str replace '  ' ' '
          }

          $env.TRANSIENT_PROMPT_COMMAND = {|| create_left_prompt_short }
          $env.TRANSIENT_PROMPT_COMMAND_RIGHT = {|| create_right_prompt }
          $env.PROMPT_COMMAND = {|| create_left_prompt_full }
          $env.PROMPT_COMMAND_RIGHT = {|| create_right_prompt }
        '';
      };
    };
  };
}


