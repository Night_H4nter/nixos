{ pkgs, globals, config, lib, sops-hm, ...}:

let
  sopsAgeKeyFile = "${globals.homeDir}/.config/sops/age/keys.txt";
  sopsSecretFile = ../../../secrets.yaml;
in

{
  options.nh.cli.secretManagement.enable = lib.mkEnableOption
    "Install packages and configs I need to manage secrets";

  config = lib.mkIf config.nh.cli.secretManagement.enable {
    sops = {
      age.keyFile = "${sopsAgeKeyFile}";
      defaultSopsFile = "${sopsSecretFile}";
    };

    home-manager = {
      sharedModules = [ sops-hm ];

      users.${globals.user} = {
        sops = {
          age.keyFile = "${sopsAgeKeyFile}";
          defaultSopsFile = "${sopsSecretFile}";
        };

        home.packages = with pkgs; [
          age
          sops
        ];
      };
    };
  };
}
