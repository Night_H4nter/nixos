{ pkgs, globals, lib, config, ... }:

{
  options.nh.tui.xplr.enable = lib.mkEnableOption {
    description = "Enable xplr with my config";
  };

  config = lib.mkIf config.nh.tui.xplr.enable {
    environment = {
      systemPackages = with pkgs; [ xplr ];
#etc.xplrConfig = {
#  source = ./global.lua;
#  target = "xplr/init.lua";
#};
    };
    home-manager.users = {
      ${globals.user}.xdg.configFile.xplr = {
        recursive = true;
        source = ./global.lua;
        target = "xplr/init.lua";
      };
      root.xdg.configFile.xplr = {
        recursive = true;
        source = ./global.lua;
        target = "xplr/init.lua";
      };
    };
  };
}
