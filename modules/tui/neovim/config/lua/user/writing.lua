vim.keymap.set({ 'n', 'v', 'i' },'<F5>', function() require("knap").process_once() end)
vim.keymap.set({ 'n', 'v', 'i' },'<F6>', function() require("knap").close_viewer() end)
vim.keymap.set({ 'n', 'v', 'i' },'<F7>', function() require("knap").toggle_autopreviewing() end)
vim.keymap.set({ 'n', 'v', 'i' },'<F8>', function() require("knap").forward_jump() end)

vim.g.knap_settings = {
  mdoutputext = "pdf",
  mdtopdfviewerlaunch = "zathura %outputfile%",
  markdownoutputext = "pdf",
  markdowntopdfviewerlaunch = "zathura %outputfile%",

  texoutputext = "pdf",
  textopdf = "xelatex -interaction=batchmode -halt-on-error -synctex=1 %docroot%",
  textopdfviewerlaunch = "zathura --synctex-editor-command 'nvim --headless -es --cmd \"lua require('\"'\"'knaphelper'\"'\"').relayjump('\"'\"'%servername%'\"'\"','\"'\"'%{input}'\"'\"',%{line},0)\"' %outputfile%",
  textopdfviewerrefresh = "none",
  textopdfforwardjump = "zathura --synctex-forward=%line%:%column%:%srcfile% %outputfile%",
}

require('orgmode').setup_ts_grammar()
require('orgmode').setup({
  org_agenda_files = {'~/Dropbox/org/*', '~/my-orgs/**/*'},
  org_default_notes_file = '~/Dropbox/org/refile.org',
})
