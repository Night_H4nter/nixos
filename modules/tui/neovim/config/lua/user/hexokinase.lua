----Hexokinase



--highlight as background
vim.g.Hexokinase_highlighters = { 'backgroundfull' }
--don't hightlight color names (they're not contextual)
vim.g.Hexokinase_optOutPatterns = { 'colour_names' }
