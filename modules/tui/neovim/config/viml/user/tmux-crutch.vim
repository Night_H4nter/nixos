"crutches to fix tmux


"rendering
autocmd VimEnter * :sleep 20m
autocmd VimEnter * :silent exec "!kill -s SIGWINCH $PPID"


"clear terminal after leaving
au VimLeave * :!clear


"not sure if it's needed or not, but it used to be needed
"to fix coloring or something
" let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
" let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
