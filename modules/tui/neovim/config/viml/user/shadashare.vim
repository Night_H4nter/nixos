""Write/read SHADA to share registers contents
""between several neovim instances

"(c) https://vi.stackexchange.com/a/24564


augroup SHADA
    autocmd!
    autocmd CursorHold,TextYankPost,FocusGained,FocusLost *
                \ if exists(':rshada') | rshada | wshada | endif
augroup END
