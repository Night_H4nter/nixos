""Both top and bottom status lines



"solarized modified colorscheme{{{
let s:cuicolors = {
      \ 'base03': [ '8', '234', 'DarkGray' ],
      \ 'base02': [ '0', '235', 'Black' ],
      \ 'base01': [ '10', '239', 'LightGreen' ],
      \ 'base00': [ '11', '240', 'LightYellow' ],
      \ 'base0':  [ '12', '244', 'LightBlue' ],
      \ 'base1':  [ '14', '245', 'LightCyan' ],
      \ 'base2': [ '7', '187', 'LightGray' ],
      \ 'base3': [ '15', '230', 'White' ],
      \ 'yellow': [ '3', '136', 'DarkYellow' ],
      \ 'orange': [ '9', '166', 'LightRed' ],
      \ 'red': [ '1', '124', 'DarkRed' ],
      \ 'magenta': [ '5', '125', 'DarkMagenta' ],
      \ 'violet': [ '13', '61', 'LightMagenta' ],
      \ 'blue': [ '4', '33', 'DarkBlue' ],
      \ 'cyan': [ '6', '37', 'DarkCyan' ],
      \ 'green': [ '2', '64', 'DarkGreen' ],
      \ }

" " The following condition only applies for the console and is the same
" " condition vim-colors-solarized uses to determine which set of colors
" " to use.
" let s:solarized_termcolors = get(g:, 'solarized_termcolors', 256)
" if s:solarized_termcolors != 256 && &t_Co >= 16
"   let s:cuiindex = 0
" elseif s:solarized_termcolors == 256
"   let s:cuiindex = 1
" else
"   let s:cuiindex = 2
" endif

let s:cuiindex = 2


let s:base03 = [ '#002b36', s:cuicolors.base03[s:cuiindex] ]
let s:base02 = [ '#073642', s:cuicolors.base02[s:cuiindex] ]
let s:base01 = [ '#586e75', s:cuicolors.base01[s:cuiindex] ]
let s:base00 = [ '#657b83', s:cuicolors.base00[s:cuiindex] ]
let s:base0 = [ '#839496', s:cuicolors.base0[s:cuiindex] ]
let s:base1 = [ '#93a1a1', s:cuicolors.base1[s:cuiindex] ]
let s:base2 = [ '#eee8d5', s:cuicolors.base2[s:cuiindex] ]
let s:base3 = [ '#fdf6e3', s:cuicolors.base3[s:cuiindex] ]
let s:yellow = [ '#b58900', s:cuicolors.yellow[s:cuiindex] ]
let s:orange = [ '#cb4b16', s:cuicolors.orange[s:cuiindex] ]
let s:red = [ '#dc322f', s:cuicolors.red[s:cuiindex] ]
let s:magenta = [ '#d33682', s:cuicolors.magenta[s:cuiindex] ]
let s:violet = [ '#6c71c4', s:cuicolors.violet[s:cuiindex] ]
let s:blue = [ '#268bd2', s:cuicolors.blue[s:cuiindex] ]
let s:cyan = [ '#2aa198', s:cuicolors.cyan[s:cuiindex] ]
let s:green = [ '#859900', s:cuicolors.green[s:cuiindex] ]

if lightline#colorscheme#background() ==# 'light'
  let [ s:base03, s:base3 ] = [ s:base3, s:base03 ]
  let [ s:base02, s:base2 ] = [ s:base2, s:base02 ]
  let [ s:base01, s:base1 ] = [ s:base1, s:base01 ]
  let [ s:base00, s:base0 ] = [ s:base0, s:base00 ]
endif

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}
let s:p.normal.left = [ [ s:base2, s:base00, 'bold' ], [ s:base2, s:base0, 'bold' ] ]
let s:p.normal.right = [ [ s:base03, s:base00, 'bold' ], [ s:base03, s:base0 ] ]
let s:p.inactive.right = [ [ s:base03, s:base00 ], [ s:base0, s:base02 ] ]
let s:p.inactive.left =  [ [ s:base0, s:base02 ], [ s:base0, s:base02 ] ]
let s:p.insert.left = [ [ s:base2, s:yellow, 'bold'  ], [ s:base2, s:base0, 'bold' ] ]
let s:p.replace.left = [ [ s:base03, s:red, 'bold'  ], [ s:base03, s:base00 ] ]
let s:p.visual.left = [ [ s:base2, s:magenta, 'bold'  ], [ s:base2, s:base0, 'bold' ] ]
let s:p.normal.middle = [ [ s:base1, s:base02 ] ]
let s:p.inactive.middle = [ [ s:base01, s:base02 ] ]
let s:p.tabline.left = [ [ s:base1, s:base02 ] ]
let s:p.tabline.tabsel = [ [ s:base2, s:base00, 'bold'  ] ]
let s:p.tabline.middle = [ [ s:base0, s:base02 ] ]
let s:p.tabline.right = copy(s:p.tabline.left)
let s:p.normal.error = [ [ s:base03, s:red, 'bold'  ] ]
let s:p.normal.warning = [ [ s:base03, s:yellow, 'bold'  ] ]

let g:lightline#colorscheme#solarized_m#palette = lightline#colorscheme#flatten(s:p)
"}}}



"selenized_dark modified colorscheme{{{
" let s:bg_1      = ['#184956', 0]
" let s:bg_2      = ['#2d5b69', 8]
" let s:dim_0     = ['#72898f', 7]
" let s:red       = ['#fa5750', 1]
" let s:green     = ['#75b938', 2]
" let s:yellow    = ['#dbb32d', 3]
" let s:blue      = ['#4695f7', 4]
" let s:magenta   = ['#f275be', 5]
" let s:cyan      = ['#41c7b9', 6]
" let s:brred     = ['#ff665c', 9]
" let s:brgreen   = ['#84c747', 10]
" let s:bryellow  = ['#ebc13d', 11]
" let s:brblue    = ['#58a3ff', 12]
" let s:brmagenta = ['#ff84cd', 13]
" let s:brcyan    = ['#53d6c7', 14]

" let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

" let s:p.normal.right = [[ s:bg_1, s:blue, 'bold' ], [ s:cyan, s:bg_2, 'bold' ], [ s:dim_0, s:bg_1, 'bold' ]]
" let s:p.normal.left = [[ s:bg_1, s:blue, 'bold' ], [ s:cyan, s:bg_2, 'bold' ]]
" let s:p.normal.middle = [[ s:dim_0, s:bg_1 ]]
" let s:p.normal.error = [[ s:bg_1, s:red ]]
" let s:p.normal.warning = [[ s:bg_1, s:yellow ]]

" let s:p.insert.right = [[ s:bg_1, s:green ], [ s:cyan, s:bg_2 ], [ s:dim_0, s:bg_1 ]]
" let s:p.insert.left = [[ s:bg_1, s:green ], [ s:cyan, s:bg_2 ]]

" let s:p.visual.right = [[ s:bg_1, s:magenta ], [ s:cyan, s:bg_2 ], [ s:dim_0, s:bg_1 ]]
" let s:p.visual.left = [[ s:bg_1, s:magenta ], [ s:cyan, s:bg_2 ]]

" let s:p.inactive.left = [[ s:brblue, s:bg_2 ], [ s:cyan, s:bg_2 ]]
" let s:p.inactive.right = [[ s:brblue, s:bg_2 ], [ s:cyan, s:bg_2 ]]

" let s:p.replace.right = [[ s:bg_1, s:red ], [ s:cyan, s:bg_2 ], [ s:dim_0, s:bg_1 ]]
" let s:p.replace.left = [[ s:bg_1, s:red ], [ s:cyan, s:bg_2 ]]

" let s:p.tabline.right = [[ s:bg_1, s:red ]]
" let s:p.tabline.left = [[ s:cyan, s:bg_2 ]]
" let s:p.tabline.tabsel = [[ s:bg_1, s:blue, 'bold' ]]

" let g:lightline#colorscheme#selenized_dark#palette = lightline#colorscheme#flatten(s:p)
"}}}




"always show tabline
" set showtabline=2


"always show statusline
set laststatus=3


"construct tabline and statusline
let g:lightline = {
            \ 'colorscheme': 'solarized_m',
            \ 'active': {
            \   'left': [
            \       [ 'mode', 'paste' ],
            \       [ 'gitstatus' ],
            \       [ 'readonly', 'filename', 'modified' ] ],
            \   'right': [
            \       [ 'lineinfo' ],
            \       [ 'percent', 'diagnostics' ],
            \       [ 'filetype' ] ]
            \ },
            \ 'component_function': {
            \   'gitstatus': 'LightLineGitStatus',
            \ },
            \ 'tabline': {
            \   'left': [ ['buffers'] ],
            \ },
            \ 'component_expand': {
            \   'buffers': 'lightline#bufferline#buffers'
            \ },
            \ 'component_type': {
            \   'buffers': 'tabsel'
            \ }
\ }





"show ordinal buffer numbers
let g:lightline#bufferline#show_number = 2


"name to use for unnamed buffers
let g:lightline#bufferline#unnamed = 'unnamed'


"enable devicons
let g:lightline#bufferline#enable_devicons = 1


"display devicons on the right
let g:lightline#bufferline#icon_position = 'right'


"compat
let g:lightline.component_raw = {'buffers': 1}
"make bufferline clickable
let g:lightline#bufferline#clickable = 1


"make lightline gitbranch module prettier
" function! LightLineGitBranch()
"     let s:branchname = gitbranch#name()
"     if len(s:branchname) != 0
"         return ' ' . s:branchname
"     else
"         return ''
"     endif
" endfunction


"get git branch
"function! s:getgitbranch()
"    return get(b:, 'gitsigns_head', '')
"endfunction


""get git status
"function! s:getgitstatus()
"    let s:gitstatus = 'A:' . get(b:, 'gitsigns_status_dict', '')['added'] . ' C:' . get(b:, 'gitsigns_status_dict', '')['changed'] . ' R:' . get(b:, 'gitsigns_status_dict', '')['removed']
"    return s:gitstatus
"endfunction


"get git branch name TODO:fix this
function! LightLineGitStatus()
let [added, modified, removed] = sy#repo#get_stats()
let l:sy = ''
for [flag, flagcount] in [
	\   [exists("g:signify_sign_add")?g:signify_sign_add:'+', added],
	\   [exists("g:signify_sign_delete")?g:signify_sign_delete:'-', removed],
	\   [exists("g:signify_sign_change")?g:signify_sign_change:'!', modified]
	\ ]
	if flagcount> 0
		let l:sy .= printf('%s%d', flag, flagcount)
	endif
endfor
if !empty(l:sy)
	let l:sy = printf('[%s]', l:sy)
	let l:sy_vcs = get(b:sy, 'updated_by', '???')
	return printf('%s%s', l:sy_vcs, l:sy)
else
	return ''
endif
endfunction



"update statusline
autocmd User BufWritePost,TextChanged,TextChangedI call lightline#update()


"export statusline into tmux
" let g:tpipeline_autoembed = 0
" update piped statusline
let g:tpipeline_cursormoved = 1
