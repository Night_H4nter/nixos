{ pkgs, globals, lib, config,... }:

{
  options.nh.tui.tmux.enable = lib.mkEnableOption {
    description = "Enable tmux with my config";
  };

  config = lib.mkIf config.nh.tui.tmux.enable {
    environment.systemPackages = with pkgs; [
      tmux
    ];

    home-manager.users.${globals.user} = {
      programs.tmux = {
        enable = true;
        secureSocket = true;
        plugins = with pkgs.tmuxPlugins; [
          yank
          resurrect
        ];
        extraConfig = builtins.readFile ./tmux.conf;
      };
    };
  };
}
