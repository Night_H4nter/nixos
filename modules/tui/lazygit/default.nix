{ pkgs, globals, lib, config, ... }:

{
  options.nh.tui.lazygit.enable = lib.mkEnableOption {
    description = "Enable lazygit with my config";
  };

  config = lib.mkIf config.nh.tui.lazygit.enable {
    home-manager.users.${globals.user} = {
      home.packages = with pkgs; [
        lazygit
      ];

      xdg.configFile.lazygit = {
        source = ./lazygit.yml;
        target = "lazygit/config.yml";
      };
    };
  };
}
