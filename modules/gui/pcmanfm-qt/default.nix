{ pkgs, globals, lib, config, ...}:

{
  options.nh.gui.pcmanfm-qt.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.gui.pcmanfm-qt.enable {
    services.gvfs.enable = true;

    home-manager.users.${globals.user}.home.packages = with pkgs; [
      lxqt.pcmanfm-qt

        #previewers
        xfce.tumbler
        webp-pixbuf-loader
        poppler
        ffmpegthumbnailer
        freetype
        libgsf
        gnome-epub-thumbnailer
        f3d
    ];
  };
}
