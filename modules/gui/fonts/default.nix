{ config, lib, pkgs, ...}:

{
  options.nh.gui.fonts.enable = lib.mkEnableOption "Enable fonts i need";

  config = lib.mkIf config.nh.gui.fonts.enable {
    #needed for ms fonts
    nixpkgs.config.allowUnfree = true;
    fonts = {
      packages = with pkgs; [
        #TODO:make an actual flake with all the fonts extracted from windows
        pkgs.corefonts
        (nerdfonts.override { fonts = [ "Hack" ]; })
      ];
      fontDir.enable = true;
    };
  };
}
