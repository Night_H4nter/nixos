{ pkgs, globals, config, lib, ... }:

{
  options.nh.gui.zathura.enable = lib.mkEnableOption {
    description = "Enable zathura with my config";
  };

  config = lib.mkIf config.nh.gui.zathura.enable {
    home-manager.users.${globals.user} = {
      home.packages = with pkgs; [
        zathura
      ];

      xdg.configFile.zathura = {
        source = ./zathurarc;
        recursive = true;
        target = "zathura/zathurarc";
      };
    };
  };
}
