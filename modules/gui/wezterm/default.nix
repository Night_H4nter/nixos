{ pkgs, globals, config, lib, ... }:

{
  options.nh.gui.wezterm.enable = lib.mkEnableOption {
    description = "Enable wezterm with my config";
  };

  config = lib.mkIf config.nh.gui.wezterm.enable {
    home-manager.users.${globals.user} = {
      home.packages = with pkgs; [
        wezterm
      ];

      xdg.configFile.wezterm = {
        source = ./config;
        recursive = true;
      };
    };
  };
}
