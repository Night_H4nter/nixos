--boilerplate{{{
-- Pull in the wezterm API
local wezterm = require 'wezterm'

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end
-- }}}


-- config.color_scheme = 'Solarized Dark Higher Contrast'
local canonical_solarized = require "solarized"
canonical_solarized.apply_to_config(config)
config.color_scheme = "Canonical Solarized Dark"

config.enable_tab_bar = false


config.harfbuzz_features = { 'calt=0', 'clig=0', 'liga=0' }
config.font = wezterm.font 'Hack Nerd Font Mono'

config.audible_bell = "Disabled"



-- config.unix_domains = {
--   {
--     name = 'unix',
--   },
-- }

-- This causes `wezterm` to act as though it was started as
-- `wezterm connect unix` by default, connecting to the unix
-- domain on startup.
-- If you prefer to connect manually, leave out this line.
-- config.default_gui_startup_args = { 'connect', 'unix' }


return config
