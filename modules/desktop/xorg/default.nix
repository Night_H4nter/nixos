{ lib, config, ... }:

{
  options.nh.desktop.xorg.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.xorg.enable {
    services.xserver = {
      enable = true;
      xkb = {
        layout = "us,ru";
        options = "grp:win_space_toggle";
      };
    };
  };
}
