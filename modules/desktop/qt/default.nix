{ lib, config, ... }:

{
  options.nh.desktop.qt.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.qt.enable {
# environment.variables = {
#   QT_QPA_PLATFORMTHEME = "lxqt";
#   XDG_CURRENT_DESKTOP = "lxqt";
#   QT_STYLE_OVERRIDE = "kvantum";
# };

    qt = {
      enable = true;
      style = "gtk2";
      platformTheme = "qt5ct";
    };


# home-manager.users.${globals.user} = {
#   qt = {
#     enable = true;
#     platformTheme = "lxqt";
#     style = {
#       # package = pkgs.nordic;
#       name = "kvantum";
#     };
#   };
# };
  };
}
