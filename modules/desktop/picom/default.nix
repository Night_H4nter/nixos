{ globals, lib, config, ... }:

{
  options.nh.desktop.picom.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.picom.enable {
    home-manager.users.${globals.user}.services.picom = {
      enable = true;
      vSync = false;
    };
  };
}
