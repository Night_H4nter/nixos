local awful = require("awful")
local wibox = require("wibox")


local shell = {}

shell.setupwidgets = function(s)

    --keybouard layout bar widget
    mykeyboardlayout = awful.widget.keyboardlayout()
    --clock/calendar bar widget
    mytextclock = wibox.widget.textclock()
    --separator
    local myseparator = wibox.widget.textbox(" ")

    --screen name
    s.myscreenname = wibox.widget{
        text = "Screen " .. s.index .. " ",
        halign = "center",
        valign = "center",
        widget = wibox.widget.textbox
    }

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()

    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox {
        screen  = s,
        buttons = {
            awful.button({ }, 1, function () awful.layout.inc( 1) end),
            awful.button({ }, 3, function () awful.layout.inc(-1) end),
        }
    }

    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = {
            awful.button({ }, 1, function(t) t:view_only() end),
            awful.button({ }, 3, awful.tag.viewtoggle),
            awful.button({ super }, 3, function(t)
                if client.focus then
                    client.focus:toggle_tag(t)
                end
            end),
            awful.button({ }, 4, function(t) awful.tag.viewprev(t.screen) end),
            awful.button({ }, 5, function(t) awful.tag.viewnext(t.screen) end),
        }
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = {
            awful.button({ }, 1, function (c)
                c:activate { context = "tasklist", action = "toggle_minimization" }
            end),
            awful.button({ }, 3, function() awful.menu.client_list { theme = { width = 250 } } end),
            awful.button({ }, 4, function() awful.client.focus.byidx(-1) end),
            awful.button({ }, 5, function() awful.client.focus.byidx( 1) end),
        }
    }

    -- Create the wibox
    s.mytopwibox = awful.wibar {
        position = "top",
        screen   = s,
        widget   = {
            layout = wibox.layout.stack,
            {
                layout = wibox.layout.align.horizontal,
                { --left
                    layout = wibox.layout.fixed.horizontal,
                    -- mylauncher,
                    s.mytaglist,
                    myseparator,
                    s.mypromptbox,
                    myseparator,
                },
                nil, --do not touch this
                { --right
                    layout = wibox.layout.fixed.horizontal,
                    myseparator,
                    mykeyboardlayout,
                    myseparator,
                    wibox.widget.systray(),
                    myseparator,
                    s.mylayoutbox,
                },
            },
            { --center
                mytextclock,
                valign = "center",
                halign = "center",
                layout = wibox.container.place
            }
        }
    }
    s.mybottobmwibox = awful.wibar {
        position = "bottom",
        screen   = s,
        widget   = {
            layout = wibox.layout.align.horizontal,
            {
                layout = wibox.layout.fixed.horizontal,
                s.mytasklist,
                myseparator,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                myseparator,
            },
            {
                layout = wibox.layout.fixed.horizontal,
                myseparator,
                s.myscreenname,
            },
        },
    }
end








return shell
