local awful = require("awful")


local hotkeys = {}

function hotkeys.setkeys(config)

    --helper function{{{
    local function tblconcat(...)
        local result = {}
        for _, t in ipairs({...}) do
            for _, v in ipairs(t) do
                table.insert(result, v)
            end
        end
        return result
    end
    -- }}}

    --mod keys
    local super = "Mod4"
    local ctrl = "Control"
    local shift = "Shift"
    local alt = "Mod1"


    --tag-related keybindings{{{
    --TODO:these are from the default rc.lua, idk if i want to rewrite them
    awful.keyboard.append_global_keybindings({
        awful.key {
            modifiers   = { super },
            keygroup    = "numrow",
            description = "only view tag",
            group       = "tag",
            on_press    = function (index)
                local screen = awful.screen.focused()
                local tag = screen.tags[index]
                if tag then
                    tag:view_only()
                end
            end,
        },
        awful.key {
            modifiers   = { super, ctrl },
            keygroup    = "numrow",
            description = "toggle tag",
            group       = "tag",
            on_press    = function (index)
                local screen = awful.screen.focused()
                local tag = screen.tags[index]
                if tag then
                    awful.tag.viewtoggle(tag)
                end
            end,
        },
        awful.key {
            modifiers = { super, shift },
            keygroup    = "numrow",
            description = "move focused client to tag",
            group       = "tag",
            on_press    = function (index)
                if client.focus then
                    local tag = client.focus.screen.tags[index]
                    if tag then
                        client.focus:move_to_tag(tag)
                    end
                end
            end,
        },
        awful.key {
            modifiers   = { super, ctrl, shift },
            keygroup    = "numrow",
            description = "toggle focused client on tag",
            group       = "tag",
            on_press    = function (index)
                if client.focus then
                    local tag = client.focus.screen.tags[index]
                    if tag then
                        client.focus:toggle_tag(tag)
                    end
                end
            end,
        }
    })
    -- }}}


    --directional keys (focuse, swap){{{
    local directionalkeybindings = {}

    local dirkeys = { "j", "k", "l", ";" }
    local dirs = { "left", "down", "up", "right" }
    for iter=1,table.getn(dirkeys) do
        local mykey = awful.key({ super }, dirkeys[iter], function()
            local dir = dirs[iter]
            awful.client.focus.global_bydirection(dir)
        end)
        table.insert(directionalkeybindings, mykey)

        local mykey = awful.key({ super, shift }, dirkeys[iter], function()
            local dir = dirs[iter]
            awful.client.swap.global_bydirection(dir)
        end)
        table.insert(directionalkeybindings, mykey)
    end
-- }}}


    --screen-related keybindings{{{
    local screenkeybindings = {}

    local scrkeys = { "u", "i", "o" }
    local screens = {  2,   1,   3  }
    for iter=1,table.getn(scrkeys) do
        local mykey = awful.key({ super }, scrkeys[iter], function()
            local tgtscreennumber = screens[iter]
            local tgtscreen = screen[tgtscreennumber]
            if tgtscreen then
                awful.screen.focus(tgtscreen)
            end
        end)
        table.insert(screenkeybindings, mykey)

        local mykey = awful.key({ super, shift }, scrkeys[iter], function()
            local tgtscreennumber = screens[iter]
            local tgtscreen = screen[tgtscreennumber]
            local currentclient = client.focus
            if currentclient then
                if tgtscreen then
                    currentclient:move_to_screen(tgtscreen)
                end
            end
        end)
        table.insert(screenkeybindings, mykey)
    end
-- }}}






    --minimize-restore{{{
    --TODO:i don't use it
    -- awful.keyboard.append_global_keybindings({
    --     awful.key({ super, ctrl }, "n",
    --               function ()
    --                   local c = awful.client.restore()
    --                   -- Focus restored client
    --                   if c then
    --                     c:activate { raise = true, context = "key.unminimize" }
    --                   end
    --               end,
    --               {description = "restore minimized", group = "client"}),
    -- })
    -- }}}





    awful.keyboard.append_global_keybinding(
        awful.key({super, shift }, "e", function()
            awful.spawn(config.prog)
        end)
    )





    --combine the keybindings generated programmatically and append them
    awful.keyboard.append_global_keybindings(
        tblconcat(
            directionalkeybindings,
            screenkeybindings
        )
        -- awful.key({"Mod4", "Shift" }, "e", function()
        --     awful.spawn(prog)
        -- end)
        -- awful.key({"Mod4", "Shift" }, "e", awesome.restart)
    )

end

return hotkeys

