{ globals, config, lib, ... }:

{
  options.nh.desktop.awesome.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.awesome.enable {
    services.xserver = {
      enable = true;

# displayManager = {
#     sddm.enable = true;
#     defaultSession = "none+awesome";
# };
# autorun = false;
      windowManager.awesome = {
        enable = true;
      };
    };


    home-manager.users.${globals.user} = {
      xsession = {
        enable = true;
        windowManager.awesome = {
          enable = true;
        };
      };

      xdg.configFile = rec {
        awesome = {
          source = ./config;
          target = "awesome";
          recursive = true;
          enable = false;
        };

# TODO:this doesn't work this way, probably fix it at some point
#make a package of some sort? idk maybe just mkderivation and then
#link the $out/init.lua into config folder, idk
# funcLuaForAwesome = {
#   source = "${functional-lua}/init.lua";
#   target = "${awesome.target}/mylib/functional-lua";
#   recursive = true;
# };
      };
    };
  };
}
