{ config, lib, ...}:

{
  options.nh.desktop.sound = {
      enable = lib.mkEnableOption {
        description = "Enable sound (WirePlumber) with my config";
        default = false;
      };
  };

  config = lib.mkIf config.nh.desktop.sound.enable {
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
      #jack.enable = true;
    };
  };
}
