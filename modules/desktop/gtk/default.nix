{ globals, pkgs, lib, config, ... }:

{
  options.nh.desktop.gtk.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.gtk.enable {
    programs.dconf.enable = true;

    home-manager.users.${globals.user} = {
      gtk = {
        enable = true;
        theme = {
          package = pkgs.solarc-gtk-theme;
          name = "SolArc-Dark";
# package = pkgs.nordic;
# name = "Nordic";
        };
# iconTheme.package = pkgs;
      };
# home.packages = with pkgs; [ lxappearance ];
    };
  };
}
