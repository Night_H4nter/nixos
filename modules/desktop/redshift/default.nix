{ globals, lib, config, ... }:

{
  options.nh.desktop.redshift.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.redshift.enable {
    home-manager.users.${globals.user}.services.redshift = {
      enable = true;
      dawnTime = "5:00-6:45";
      duskTime = "16:35-20:15";
      temperature = {
        day = 6000;
        night = 4500;
      };
      tray = true;
# settings = {

# };
    };
  };
}
