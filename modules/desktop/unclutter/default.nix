{ globals, config, lib, ... }:

{
  options.nh.desktop.unclutter.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.tui.xplr.enable {
    home-manager.users.${globals.user} = {
      services.unclutter = {
        enable = true;
        threshold = 5;
        timeout = 3;
      };
    };
  };
}
