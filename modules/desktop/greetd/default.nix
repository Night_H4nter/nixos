{ pkgs, globals, lib, config, ... }:

{
  options.nh.desktop.greetd.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.greetd.enable {
    services.xserver.displayManager.startx.enable = true;

    services.greetd = {
      enable = true;
      settings = {
        default_session = {
          command = "${pkgs.greetd.tuigreet}/bin/tuigreet --time --cmd startx";
          user = "nh";
        };
      };
    };

    home-manager.users.${globals.user}.home.file.".xinitrc" = {
      enable = true;
      executable = true;
      text = ''
#!${pkgs.bash}/bin/bash
set -e

dbus-daemon --session --address="unix:path=$XDG_RUNTIME_DIR/bus" &
systemctl --user import-environment DISPLAY XAUTHORITY
if command -v dbus-update-activation-environment >/dev/null 2>&1; then
  dbus-update-activation-environment DISPLAY XAUTHORITY
fi

${pkgs.systemd}/bin/systemctl --user start nixos-fake-graphical-session.target

exec ${pkgs.awesome}/bin/awesome > ${globals.homeDir}/.local/awesomelog
'';
    };
  };
}
