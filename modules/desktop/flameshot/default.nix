{ globals, lib, config, ...}:

{
  options.nh.desktop.flameshot.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.flameshot.enable {
    home-manager.users.${globals.user} = {
      services.flameshot.enable = true;

      xdg.configFile.flameshot = {
        source = ./flameshot.ini;
        recursive = true;
        target = "flameshot/flameshot.ini";
      };
    };
  };
}
