{ pkgs, globals, lib, config, ... }:

let
  screenLockScript = pkgs.nuenv.writeScriptBin {
    name = "xlockscreen";
    script = ''
      try {
        ${pkgs.xkb-switch}/bin/xkb-switch -s us
      } catch {
        (
          ${pkgs.libnotify}/bin/notify-send
            -u critical -a "Screen Locker"
            "Could not switch the keyboard layout to English (US). You will be unable to unlock the session. Aborting the locking procedure."
        )
      }
      try {
        (
          ${pkgs.i3lock-color}/bin/i3lock-color -c 002b36
          --indicator
          --radius=176
          --inside-color=002b36
          --insidever-color=002b36
          --insidewrong-color=002b36
          --ring-color=eee8d5
          --ringver-color=2aa198
          --ringwrong-color=dc322f
          --ring-width=5.0
          --line-uses-inside
          --keyhl-color=2aa198
          -k
          --time-color=eee8d5
          --time-str="%H:%M"
          --time-size=56
          --date-color=eee8d5
          --date-str="%A, %d.%m.%Y"
          --date-size=24
          --verif-text="Checking..."
          --verif-color=2aa198
          --greeter-color=2aa198
          --wrong-color=dc322f
        )
      } catch {
        (
          ${pkgs.libnotify}/bin/notify-send
            -u critical -a "Screen Locker" "Screen Locking failed."
        )
      }
    '';
  };
  screenLockCmd = "${screenLockScript}/bin/xlockscreen";

in

{
  options.nh.desktop.hotkeys.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.hotkeys.enable {
    home-manager.users.${globals.user} = {
      # services.sxhkd = {
      #   enable = false;
      #   keybindings = {
      #     "super + backslash" = "${screenLockCmd}";
      #     "alt + {q,w}" = "${pkgs.xkb-switch}/bin/xkb-switch -s {us,ru}";
      #     "super + b" = "${globals.shitBrowser}";
      #     "super + Return" = "${globals.terminal}";
      #     "{XF86AudioLowerVolume, XF86AudioRaiseVolume}" = "${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_SINK@ 3%{-,+}";
      #     "super + grave" = "flameshot gui";
      #   };
      # };

      xdg.configFile.sxhkdrc = {
        enable = true;
        target = "sxhkd/sxhkdrc";
        text = ''
super + backslash
	${screenLockCmd}
alt + {q,w}
	${pkgs.xkb-switch}/bin/xkb-switch -s {us,ru}
super + b
	${globals.shitBrowser}
super + Return
	${globals.terminal}
{XF86AudioLowerVolume, XF86AudioRaiseVolume}
	${pkgs.wireplumber}/bin/wpctl set-volume @DEFAULT_SINK@ 3%{-,+}
super + grave
	flameshot gui
super + c
  clipmenu
        '';
      };

      systemd.user.services.sxhkd-srv = {
        Unit = {
          Description = "sxhkd, but with a proper systemd service";
          Documentation = [ "man:sxhkd(1)" ];
        };

        Install = {
          WantedBy = [ "graphical-session.target" ];
        };

        Service = {
          After = [ "graphical-session.target" ];
          ExecStart = "${pkgs.sxhkd}/bin/sxhkd";
        };
      };
    };
  };
}
