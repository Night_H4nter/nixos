{ pkgs, globals, ... }:

{
  home-manager.users.${globals.user}.home.packages = with pkgs; [
    #dolphin itself
    libsForQt5.dolphin

    #addons
    libsForQt5.dolphin-plugins
    libsForQt5.breeze-icons

    #previews
    libsForQt5.kdegraphics-thumbnailers
    libsForQt5.kimageformats
    libsForQt5.qt5.qtimageformats
    libsForQt5.ffmpegthumbs
    qt6.qtimageformats
    libheif
    resvg
    taglib

    #kio slaves
    kio-fuse
    libsForQt5.kio-extras
    libsForQt5.kio-gdrive
    libsForQt5.kio-admin
  ];
}
