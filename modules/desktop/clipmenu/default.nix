{ globals, pkgs, lib, config, ... }:

{
  options.nh.desktop.clipmenu.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.desktop.clipmenu.enable {
    home-manager.users.${globals.user} = {
      services.clipmenu = {
        enable = true;
        launcher = "rofi";
      };

      home.packages = with pkgs; [
        rofi
          xsel
          clipnotify
      ];

#i'm too lazy to file a bug report on clipmenu not reading launcher
#variable contents
      home.sessionVariables = {
        CM_LAUNCHER = "rofi";
      };
      # programs.nushell.environmentVariables = {
      #   CM_LAUNCHER = "rofi";
      # };

# services.sxhkd.keybindings = {
#   "super + c" = "clipmenu";
# };
    };
  };
}
