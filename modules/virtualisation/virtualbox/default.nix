{ config, lib, globals, pkgs-vbox, ... }:

{
  options.nh.virtualisation.virtualbox.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.virtualisation.virtualbox.enable {
    #needed for vbox extensions
    nixpkgs.config.allowUnfree = true;
    users.extraGroups.vboxusers.members = [ "${globals.user}" ];

    virtualisation.virtualbox.host = {
      package = pkgs-vbox.virtualbox;
      enable = true;
      enableExtensionPack = true;
    };

    #this is needed because without this module loaded, one of the systemd
    #services refuses to reload, and it may break the procedure of
    #switching to new configuration
    system.activationScripts.loadVboxModule.text = ''
${pkgs-vbox.kmod}/bin/modprobe vboxdrv
    '';
  };
}

