{ config, lib, pkgs, ... }:

{
  options.nh.virtualisation.docker.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.virtualisation.docker.enable {
    virtualisation.docker = {
      enable = true;
    };

    environment.systemPackages = with pkgs; [
      dive
      docker-compose
    ];
  };
}
