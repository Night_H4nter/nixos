{ pkgs, config, lib, ... }:

{
  options.nh.virtualisation.podman.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.virtualisation.podman.enable {
    virtualisation = {
      containers.enable = true;
      podman = {
        enable = true;
        defaultNetwork.settings.dns_enabled = true;
      };
    };

    environment.systemPackages = with pkgs; [
      dive
      podman-compose
    ];
  };
}
