{ config, lib, globals, pkgs, ...}:

let
  virshBinary = "${pkgs.libvirt}/bin/virsh";
  grepBinary = "${pkgs.gnugrep}/bin/grep";
in
{
  options.nh.virtualisation.qemukvm.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.virtualisation.qemukvm.enable {
    programs.dconf.enable = true;

    virtualisation.libvirtd = {
      enable = true;
      qemu = {
        swtpm.enable = true;
        ovmf = {
          enable = true;
          packages = [(pkgs.OVMF.override {
            secureBoot = true;
            tpmSupport = true;
            }).fd];
        };
      };
    };

    users.users.${globals.user}.extraGroups = [ "libvirtd" ];

    home-manager.users.${globals.user} = {
      dconf.settings = {
        "org/virt-manager/virt-manager/connections" = {
          autoconnect = ["qemu:///system"];
          uris = ["qemu:///system"];
        };
        "org/virt-manager/virt-manager/new-vm" = {
          firmware = "uefi";
        };
      };

      home.packages = with pkgs; [
        virt-manager
        gnome.gnome-boxes
      ];
    };

    system.activationScripts.startVirtualNetwork.text = ''
#only run if default network isn't running
${virshBinary} net-info default |
  ${grepBinary} Active |
  ${pkgs.gawk}/bin/awk '{ print $2 }' |
  ${grepBinary} -qFx 'no' && {
    ${virshBinary} net-start default
}
    '';
  };
}
