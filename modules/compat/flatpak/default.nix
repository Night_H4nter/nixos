#for now, i only use packages from flathub and don't need to pin anything,
#and that's why i don't have any custom logic to handle that,
#so just hard code stuff like this, it shouldn't be difficult to write
#additional logic later

{ pkgs, globals, config, lib, ... }:

let
  listToString = inList: pkgs.lib.strings.concatStringsSep " " inList;
  flatpakBin = "${pkgs.flatpak}/bin/flatpak";
  flatpakUpdateCmd = "${flatpakBin} update --noninteractive";
in
{
  options.nh.compat.flatpak = {
    enable = lib.mkEnableOption "Enable nh flatpak setup";

    packages = lib.mkOption {
      type = lib.types.listOf lib.types.str;
      description = "List of full flatpak ids to install";

      example = lib.literalExpression ''
      [
        # declare apps to install by fqdn
        "org.mozilla.firefox"
        "com.github.tchx84.Flatseal"
      ]
      '';

      #this is what i use
      default = [
        "org.mozilla.firefox"
        "com.google.Chrome"
        # "com.microsoft.Edge"
        "com.github.tchx84.Flatseal"
        "com.usebottles.bottles"
        "com.discordapp.Discord"
        "org.telegram.desktop"
      ];
    };

    autoUpdate = lib.mkEnableOption "Enable automatic weekly flatpak update";
  };

  config =  lib.mkIf config.nh.compat.flatpak.enable {
    xdg.portal = lib.mkIf config.nh.compat.flatpak.enable {
      enable = true;
      #list of all portal names:
      #https://github.com/flatpak/xdg-desktop-portal/tree/main/data
      config = {
        common = {
          "org.freedesktop.impl.portal.FileChooser" = [ "lxqt" ];
          default = [ "kde" ];
        };
      };
      extraPortals = with pkgs; [
        xdg-desktop-portal-kde
          lxqt.xdg-desktop-portal-lxqt
      ];
    };

    #needed for xdg desktop portal
    #https://github.com/NixOS/nixpkgs/issues/189851#issuecomment-1238907955
    # systemd.user.extraConfig = ''
    #   DefaultEnvironment="PATH=/run/current-system/sw/bin"
    # '';
    systemd.user.extraConfig = lib.mkIf config.nh.compat.flatpak.enable ''
      DefaultEnvironment="PATH=/run/wrappers/bin:/etc/profiles/per-user/%u/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin"
      '';
    environment.systemPackages = lib.mkIf config.nh.compat.flatpak.enable (with pkgs; [
      libsForQt5.kcoreaddons
      libsForQt5.breeze-icons
    ]);
    # environment.variables = { XDG_CURRENT_DESKTOP = "KDE"; };

    services.flatpak.enable = lib.mkIf config.nh.compat.flatpak.enable true;

    system.activationScripts.setupFlatpak.text =
      lib.mkIf config.nh.compat.flatpak.enable
''
#only run if there's network available (e.g. not when booting)
${pkgs.iputils}/bin/ping flathub.org -c1 >/dev/null 2>&1 && {

${flatpakBin} remote-add --if-not-exists flathub \
https://dl.flathub.org/repo/flathub.flatpakrepo

${flatpakBin} install --noninteractive flathub \
${ listToString config.nh.compat.flatpak.packages }

}
'';

    systemd = {
      timers.nh-flatpak-autoupdate = {
        unitConfig = {
          Description = "Automatically update all flatpak packages every saturday";
        };
        timerConfig = {
          OnCalendar = "Sat 19:00:00";
          Persistent = true;
        };

        enable = lib.mkIf config.nh.compat.flatpak.autoUpdate true;
      };

      services.nh-flatpak-autoupdate = {
        unitConfig = {
          Description = "Flatpak autoupdate service";
          Documentation = [ "man:flatpak-update(1)" ];
        };
        serviceConfig = {
          ExecStart = "${flatpakUpdateCmd}";
        };
      };
    };
  };
}
