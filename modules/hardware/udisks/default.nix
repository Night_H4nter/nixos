{ config, lib, ... }:

{
  options.nh.hardware.udisks.enable = lib.mkEnableOption {};

  config = lib.mkIf config.nh.hardware.udisks.enable {
    services.udisks2 = {
      enable = true;
    };
  };
}

