
(wezterm-git: final: prev: {
  # awesome-luajit = prev.awesome.override { lua = prev.luajit; };

  wezterm = prev.wezterm.overrideAttrs {
    src = wezterm-git;
  };
})

