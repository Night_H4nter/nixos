#build awesomewm from git and run it with luajit

#relies on inputs.awesomewm-git to function, see flake.nix in this repo

#if anything breaks, look for changes in nixos package declaration and 
#breaking commits on awesomem github


(awesomewm-git: final: prev: {
  awesome-luajit = prev.awesome.override { lua = prev.luajit; };

  awesome = final.awesome-luajit.overrideAttrs {
    src = awesomewm-git;

    patches = [ ];

    postPatch = ''
      patchShebangs tests/examples/_postprocess.lua
    '';
  };
})

