{ globals, ... }:

{
  imports =
    [
      ./hardware.nix
      ./storage.nix
      ./services.nix
    ];

  system.stateVersion = "24.05";

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "jbod";
  time.timeZone = "Europe/Saratov";

  services = {
    openssh = {
      enable = true;
      startWhenNeeded = true;
      settings = {
        PermitRootLogin = "no";
        PasswordAuthentication = false;
      };
      openFirewall = true;
      allowSFTP = true;
    };
  };

  users.users.${globals.user}.openssh.authorizedKeys.keys = [
    "${globals.pubkeys.jbod}"
  ];

  services.logind = {
    rebootKey = "ignore";
    powerKey = "ignore";
  };

  nh = {
    # services.qbittorrent.enable = true;
  };
}
