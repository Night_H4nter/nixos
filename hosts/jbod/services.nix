{ ... }:

let
  torrent-downloads-dir = "/data/junk/torrentdata";
  torrent-watch-dir = "/data/junk/torrentwatched";
  svcdata-path = "/data/svcdata";
  storage-path = "/data/storage";
in
{
#TODO:maybe hide all management and nfs in a wireguard network
  services.transmission = {
    enable = true;
    openPeerPorts = true;
    openRPCPort = true;
    settings = {
      download-dir = "${torrent-downloads-dir}";
      #TODO:automate its creation?
      incomplete-dir = "${torrent-downloads-dir}/.incomplete";
      watch-dir = "${torrent-watch-dir}";
      watch-dir-enabled = true;
      rpc-bind-address = "0.0.0.0";
      rpc-host-whitelist-enabled = "false";
      rpc-whitelist-enabled = "false";
      # rpc-host-whitelist = "";
    };
  };

  services.nfs.server = {
    enable = true;
    lockdPort = 4001;
    mountdPort = 4002;
    statdPort = 4000;
    extraNfsdConfig = '''';
    exports = ''
      ${torrent-downloads-dir}  192.168.88.0/24(rw,nohide,insecure)
      ${torrent-watch-dir}      192.168.88.0/24(rw,nohide,insecure)
      ${svcdata-path}           192.168.88.0/24(rw,nohide,insecure)
      ${storage-path}           192.168.88.0/24(rw,nohide,insecure)
      '';
  };
  networking.firewall = {
    allowedTCPPorts = [ 111 2049 4000 4001 4002 20048 ];
    allowedUDPPorts = [ 111 2049 4000 4001 4002 20048 ];
  };

}
