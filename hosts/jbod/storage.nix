{ config, pkgs, ... }:

{
  #zfs
  networking.hostId = "6d6a7e0b";

  boot = {
    kernelPackages =
      config.boot.zfs.package.latestCompatibleLinuxPackages;
    supportedFilesystems = [ "zfs" ];
    zfs.forceImportRoot = false;
  };

  services.zfs = {
    autoScrub = {
      enable =  true;
      interval = "weekly";
    };

    autoSnapshot = {
      enable = true;
      frequent = 12;
      daily = 15;
      weekly = 3;
      monthly = 2;
    };
  };

  services.smartd.enable = true;

  fileSystems."/data/storage" = {
    device = "zpool-wd-mirror/storage";
    fsType = "zfs";
    options = [ "nodev" "nosuid" "noexec" "nofail" ];
  };

  fileSystems."/data/svcdata" = {
    device = "zpool-wd-mirror/svcdata";
    fsType = "zfs";
    options = [ "nodev" "nosuid" "noexec" "nofail" ];
  };

  fileSystems."/data/junk" = {
    device = "/dev/disk/by-id/ata-WDC_WD20EZAZ-00L9GB0_WD-WX42D32JF9CD-part1";
    options = [ "nodev" "nosuid" "noexec" "noatime" "nofail" ];
    fsType = "ext4";
  };

  fileSystems."/data/backups" = {
    device = "/dev/disk/by-id/ata-TOSHIBA_HDWN180_79FYK0P0FAVG-part1";
    options = [ "nodev" "nosuid" "noexec" "noatime" "nofail" ];
    fsType = "ext4";
  };

  # system.activationScripts.setStoragePermissions.text = ''
# ${pkgs.coreutils-full}/bin/chown -R nobody:wheel /data/torrents
# ${pkgs.coreutils-full}/bin/chmod -R 774 /data/torrents

# ${pkgs.coreutils-full}/bin/chown -R nh:wheel /data/storage
  # '';
}
