{ config, globals, ... }:

{
  imports = [
    ./hardware-configuration.nix
  ];

  networking.hostName = "nhdpc";

  fileSystems."/data/torrents" = {
    device = "192.168.88.249:/data/junk/torrentdata";
    fsType = "nfs";
    options = [ "rw" "noatime" "nofail" "nfsvers=4" "x-systemd.automount" "noauto" ];
  };
  fileSystems."/data/autotorrent" = {
    device = "192.168.88.249:/data/junk/torrentwatched";
    fsType = "nfs";
    options = [ "rw" "noatime" "nofail" "nfsvers=4" "x-systemd.automount" "noauto" ];
  };
  fileSystems."/data/services" = {
    device = "192.168.88.249:/data/svcdata";
    fsType = "nfs";
    options = [ "rw" "noatime" "nofail" "nfsvers=4" "x-systemd.automount" "noauto" ];
  };
  fileSystems."/data/storage" = {
    device = "192.168.88.249:/data/storage";
    fsType = "nfs";
    options = [ "rw" "noatime" "nofail" "nfsvers=4" "x-systemd.automount" "noauto" ];
  };

  nh = {
    cli = {
      git.enable = true;
      nushell.enable = true;
      secretManagement.enable = true;
      ssh = {
        jbod-key.enable = true;
      };
    };

    tui = {
      lazygit.enable = true;
      xplr.enable = true;
      neovim.enable = true;
      tmux.enable = true;
    };

    desktop = {
      greetd.enable = true;
      xorg.enable = true;
      awesome.enable = true;
      redshift.enable = true;
      hotkeys.enable = true;
      unclutter.enable = true;
      picom.enable = true;
      qt.enable = true;
      gtk.enable = true;
      sound.enable = true;
      flameshot.enable = true;
      clipmenu.enable = true;
      polkit.enable = true;
    };

    gui = {
      fonts.enable = true;
      pcmanfm-qt.enable = true;
      zathura.enable = true;
      wezterm.enable = true;
    };

    hardware = {
      udisks.enable = true;
    };

    compat = {
      flatpak = {
        enable = true;
        autoUpdate = true;
      };
    };

    virtualisation = {
      virtualbox.enable = true;
      qemukvm.enable = true;
      docker.enable = true;
      podman.enable = true;
    };
  };

  #additional user conf
  users.users.${globals.user} = {
    isNormalUser = true;
    description = "${globals.fullName}";
    extraGroups = [
      "networkmanager"
      "wheel"
      # "kvm"
      # "libvirt"
      "adbusers"
    ];
    initialPassword = "12";
    home = "${globals.homeDir}";
    # subUidRanges = [{ startUid = 100000; count = 65536; }];
    # subGidRanges = [{ startGid = 100000; count = 65536; }];
  };


  hardware.keyboard.qmk.enable = true;


  #power management
  services.logind = {
    rebootKey = "ignore";
    powerKey = "ignore";
  };


  # fileSystems."/data" = {
  #   device = "/dev/disk/by-label/DATAPART";
  #   fsType = "ext4";
  # };


  #boot
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub.enable = true;
  boot.loader.grub.devices = [ "nodev" ];
  boot.loader.grub.efiInstallAsRemovable = true;
  boot.loader.grub.efiSupport = true;
  boot.loader.grub.useOSProber = true;
  boot.supportedFilesystems = [ "zfs" ];
  boot.zfs.requestEncryptionCredentials = true;
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.kernelParams = [ "quiet" ];

  networking.hostId = "c642da8b";

  services.zfs.autoScrub.enable = true;

  time.timeZone = "Europe/Saratov";
  system.stateVersion = "23.11";
}

