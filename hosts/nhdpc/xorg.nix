{ globals, ...}:
{
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  hardware.opengl = {
    enable = true;
    driSupport = true;
    driSupport32Bit = true;
  };

  services.xserver.videoDrivers = ["nvidia"];
  hardware.nvidia = {
    modesetting.enable = true;
    nvidiaSettings = true;
  };

  # services.autorandr = {
  home-manager.users.${globals.user} = {
    services.autorandr.enable = true;
    programs.autorandr = {
      enable = true;
      profiles = {
        home = {
          fingerprint = {
            DP-0 = "00ffffffffffff0006b3af241579010033200104a5351e783b51b5a4544fa0260d5054bfcf00814081809500714f81c0b30001010101023a801871382d40582c45000f282100001e0882805070384d400820f80c0f282100001a000000fd003090b4b422010a202020202020000000fc00415355532056503234390a20200137020330f14d010304131f120211900e0f1d1e230907078301000067030c00100000446d1a000002013090000000000000fe5b80a070383540302035000f282100001a866f80a070384040302035000f282100001a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000b3";
            DP-4 = "00ffffffffffff0006b3af241c79010033200104a5351e783b51b5a4544fa0260d5054bfcf00814081809500714f81c0b30001010101023a801871382d40582c45000f282100001e0882805070384d400820f80c0f282100001a000000fd003090b4b422010a202020202020000000fc00415355532056503234390a20200130020330f14d010304131f120211900e0f1d1e230907078301000067030c00100000446d1a000002013090000000000000fe5b80a070383540302035000f282100001a866f80a070384040302035000f282100001a00000000000000000000000000000000000000000000000000000000000000000000000000000000000000b3";
          };
          config = {
            DP-0 = {
              enable = true;
              crtc = 1;
              position = "0x0";
              mode = "1920x1080";
              rate = "143.85";
            };
            DP-4 = {
              enable = true;
              crtc = 0;
              position = "1920x0";
              mode = "1920x1080";
              rate = "143.85";
              primary = true;
            };
          };
        };
      };
    };
  };

}
